.PHONY: api
api:
	docker-compose run --service-ports --rm api

.PHONY: yay
yay:
	docker-compose run --service-ports --rm yay

stop:
	docker-compose down